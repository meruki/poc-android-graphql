package com.example.pocgraphql.view

import android.app.ProgressDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.activity.viewModels
import com.example.pocgraphql.R
import com.example.pocgraphql.viewmodel.MainViewModel

class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModels()
    private var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel.dismissDialogLiveData.observe(this) {
            progressDialog?.dismiss()
        }

        findViewById<Button>(R.id.getButton).setOnClickListener {
            showLoading("Getting, please wait...")
            viewModel.getUserById()
        }
        findViewById<Button>(R.id.postButton).setOnClickListener {
            showLoading("Posting, please wait...")
            viewModel.createUser()
        }
        findViewById<Button>(R.id.graphQLButton).setOnClickListener {
            showLoading("Fetching, please wait...")
            viewModel.fetchBooks()
        }

    }

    private fun showLoading(msg: String) {
        progressDialog = ProgressDialog.show(this, null, msg, true)
    }
}