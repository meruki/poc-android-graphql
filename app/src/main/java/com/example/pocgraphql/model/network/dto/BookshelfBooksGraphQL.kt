package com.example.pocgraphql.model.network.dto

data class BookshelfBooksGraphQL(
    val data: Data
) {
    companion object {
        fun query(personId: String): String {
            return """
        query Books {
            Books {
                edges {
                    node {
                        id
                        externalId
                        title
                        description
                        edition
                        bookType
                        yearPublication
                        numberPages
                        covers {
                            edges {
                                node {
                                    coverPath
                                }
                            }
                        }
                        files {
                            edges {
                                node {
                                    id
                                    version
                                    fileLength
                                    fileType
                                    filePath
                                }
                            }
                        }
                        totalOedsLength
                        authors {
                            edges {
                                node {
                                    id
                                    name
                                }
                            }
                        }
                        grades {
                            edges {
                                node {
                                    id
                                    description
                                }
                            }
                        }
                        publishingHouse {
                            id
                            description
                        }
                        subjectTypes {
                            edges {
                                node {
                                    id
                                    externalId
                                    name
                                }
                            }
                        }
                        collections {
                            edges {
                                node {
                                    id
                                    name
                                    description
                                }
                            }
                        }
                        mappings(query: "bundle.ownerships.personId==${personId}") {
                            edges {
                                node {
                                    bundle {
                                        ownerships(query: "personId==${personId}") {
                                            edges {
                                                node {
                                                    expired
                                                    expirationDate
                                                    suspended
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        allowOedDownload
                        primaryInfo
                        secondaryInfo
                        pageRecognitionEnabled
                        learningLevels {
                            edges {
                                node {
                                    id
                                    description
                                }
                            }
                        }
                    }
                }
            }
        }
        """
        }
    }

    data class Data(
        val Books: _Books
    ) {
        data class _Books (
            val edges: List<Edges>
        ) {
            data class Edges(
                val node: Node
            ) {
                data class Node(
                    val id: String?,
                    val externalId: String?,
                    val title: String?,
                    val description: String?,
                    val edition: String?,
                    val bookType: String?,
                    val yearPublication: String?,
                    val numberPages: String?,
                    val covers: Covers?,
                    val files: Files?,
                    val totalOedsLength: Long?,
                    val authors: Authors?,
                    val grades: Grades?,
                    val publishingHouse: PublishingHouse?,
                    val subjectTypes: SubjectTypes?,
                    val collections: Collections?,
                    val mappings: Mappings?,
                    val allowOedDownload: Boolean?,
                    val primaryInfo: String?,
                    val secondaryInfo: String?,
                    val pageRecognitionEnabled: Boolean?,
                    val learningLevels: LearningLevels?
                ) {
                    data class Covers(
                        val edges: List<Edges>
                    ) {
                        data class Edges(
                            val node: Node
                        ) {
                            data class Node(
                                val coverPath: String?
                            )
                        }
                    }

                    data class Files(
                        val edges: List<Edges>
                    ) {
                        data class Edges(
                            val node: Node
                        ) {
                            data class Node(
                                val id: String?,
                                val version: Int?,
                                val fileLength: Int?,
                                val fileType: String?,
                                val filePath: String?
                            )
                        }
                    }

                    data class Authors(
                        val edges: List<Edges>
                    ) {
                        data class Edges(
                            val node: Node
                        ) {
                            data class Node(
                                val id: String?,
                                val name: String?
                            )
                        }
                    }

                    data class Grades(
                        val edges: List<Edges>
                    ) {
                        data class Edges(
                            val node: Node
                        ) {
                            data class Node(
                                val id: String?,
                                val description: String?
                            )
                        }
                    }

                    data class PublishingHouse(
                        val id: String?,
                        val description: String?
                    )

                    data class SubjectTypes(
                        val edges: List<Edges>
                    ) {
                        data class Edges(
                            val node: Node
                        ) {
                            data class Node(
                                val id: String?,
                                val externalId: String?,
                                val name: String?
                            )
                        }
                    }

                    data class Collections(
                        val edges: List<Edges>
                    ) {
                        data class Edges(
                            val node: Node
                        ) {
                            data class Node(
                                val id: String?,
                                val name: String?,
                                val description: String?
                            )
                        }
                    }

                    data class Mappings(
                        val edges: List<Edges>
                    ) {
                        data class Edges(
                            val node: Node
                        ) {
                            data class Node(
                                val bundle: Bundle
                            ) {
                                data class Bundle(
                                    val ownerships: Ownerships
                                ) {
                                    data class Ownerships(
                                        val edges: List<Edges>
                                    ) {
                                        data class Edges(
                                            val node: Node
                                        ) {
                                            data class Node(
                                                val expired: Boolean?,
                                                val expirationDate: String?,
                                                val suspended: Boolean?
                                            )
                                        }
                                    }
                                }
                            }
                        }
                    }

                    data class LearningLevels(
                        val edges: List<Edges>
                    ) {
                        data class Edges(
                            val node: Node
                        ) {
                            data class Node(
                                val id: String?,
                                val description: String?
                            )
                        }
                    }
                }
            }
        }
    }
}