package com.example.pocgraphql.model.repositories

import android.util.Log
import com.example.pocgraphql.model.network.RetrofitHelper
import com.example.pocgraphql.model.network.api.ApiService
import com.example.pocgraphql.model.network.dto.BaseResponse
import com.example.pocgraphql.model.network.dto.BookshelfBooksGraphQL
import com.example.pocgraphql.model.network.dto.UserResponse
import com.google.gson.JsonObject

class Repository {

    private var apiService: ApiService = RetrofitHelper.getInstance().create(ApiService::class.java)
    val tag = "ASD&A%SD"

    suspend fun getUserByID(id: String): BaseResponse<UserResponse>? {
        return try {
            apiService.getUserByID(id)
        } catch (e: Exception) {
            Log.d(tag, "GET Failure: ${e.message}")
            null
        }
    }

    suspend fun createUser(jsonObject: JsonObject): JsonObject? {
        return try {
            apiService.createUser(jsonObject)
        } catch (e: Exception) {
            Log.d(tag, "POST Failure: ${e.message}")
            null
        }
    }

    suspend fun fetchBooks(token: String, body: JsonObject): BookshelfBooksGraphQL? {
        return try {
            apiService.fetchBooks(token, body)
        } catch (e: Exception) {
            Log.d(tag, "GraphQL Failure: Error ${e.message}")
            null
        }
    }
}