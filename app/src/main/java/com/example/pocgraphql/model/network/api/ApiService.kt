package com.example.pocgraphql.model.network.api

import com.example.pocgraphql.model.network.dto.BaseResponse
import com.example.pocgraphql.model.network.dto.BookshelfBooksGraphQL
import com.example.pocgraphql.model.network.dto.UserResponse
import com.google.gson.JsonObject
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Path

interface ApiService {

    @GET("/api/users/{id}")
    suspend fun getUserByID(@Path("id") id: String): BaseResponse<UserResponse>

    @POST("/api/users")
    suspend fun createUser(@Body body: JsonObject): JsonObject

    @POST("https://aps.s.plurall.net/tmp/graphql")
    suspend fun fetchBooks(@Header("Authorization") token: String, @Body body: JsonObject): BookshelfBooksGraphQL

}