package com.example.pocgraphql.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pocgraphql.model.network.dto.BookshelfBooksGraphQL
import com.example.pocgraphql.model.repositories.Repository
import com.google.gson.JsonObject
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    private val repository: Repository = Repository()
    private val tag = "ASD&A%SD"

    val dismissDialogLiveData: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>(false)
    }

    fun getUserById() {
        viewModelScope.launch {
            val result = repository.getUserByID("2")
            if (result != null) {
                Log.d(tag, "GET Success: ${result.data?.firstName}")
            }
            dismissDialogLiveData.postValue(true)
        }
    }

    fun createUser() {
        viewModelScope.launch {
            val body: JsonObject = JsonObject().apply {
                addProperty("name", "vichit coding delivery")
                addProperty("job", "Android Developer")
            }

            val result = repository.createUser(body)
            if (result != null) {
                Log.d(tag, "POST Success: $result")
            }
            dismissDialogLiveData.postValue(true)
        }
    }

    fun fetchBooks() {
        viewModelScope.launch {
            val token = "Bearer 897284f3e5d4a27c7ddc81fb4fd6d80a"
            val personId = "7806945"
            val body: JsonObject = JsonObject().apply {
                addProperty("query", BookshelfBooksGraphQL.query(personId))
            }

            Log.d(tag, "personId: $personId")

            val result = repository.fetchBooks(token, body)
            if (result != null) {
                Log.d(tag, "GraphQL Success: ${result.data}")
            }
            dismissDialogLiveData.postValue(true)
        }
    }

}